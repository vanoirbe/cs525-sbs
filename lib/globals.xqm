xquery version "3.1";
(: --------------------------------------------------------------
   SBS application
   
   Global information about application

   Contributor(s): Christine Vanoirbeek

   July 2019 
   -------------------------------------------------------------- :)

module namespace globals = "http://sbs.com/globals";

(: Application name (rest), project folder name and application collection name :)

declare variable $globals:app-name := 'cs525-sbs';
declare variable $globals:app-folder := 'projects';
declare variable $globals:app-collection := 'sbs';

(: Configuration paths :)

declare variable $globals:mapping-resource :='/db/www/sbs/config/mapping.xml';
declare variable $globals:dico-collection := '/db/www/sbs/dictionaries';
declare variable $globals:database-entities-resource := '/db/www/sbs/config/database.xml';
declare variable $globals:schema-collection :='/db/www/sbs/validation/schema';
declare variable $globals:global-schema-resource :='/db/www/sbs/validation/sbs.rng';
declare variable $globals:generate-all-schema-transfo :='/db/www/sbs/validation/generate-all-schema.xsl';
declare variable $globals:templates-uri := '/db/www/sbs/templates';
declare variable $globals:application-uri := '/db/www/sbs/config/application.xml';

(: Application path :)

declare variable $globals:global-information-collection :="/db/sites/sbs/global-information";
declare variable $globals:access-rigths-resource :="/db/sites/sbs/global-information/access-rights.xml";
declare variable $globals:navigation-resource :="/db/sites/sbs/global-information/navigation.xml";
declare variable $globals:selectors-resource :='/db/sites/sbs/global-information/selectors.xml';
declare variable $globals:ids-resource :='/db/sites/sbs/global-information/ids.xml';

declare variable $globals:topics-collection :="/db/sites/sbs/topics";
declare variable $globals:home-resource := '/db/sites/sbs/topics/home.xml';

declare variable $globals:spaces-collection :="/db/sites/sbs/spaces";

declare variable $globals:users-collection :='/db/sites/sbs/users';


declare variable $globals:globals-uri := '/db/www/sbs/config/globals.xml';

declare function globals:app-name() as xs:string {
  $globals:app-name
};

declare function globals:app-folder() as xs:string {
  $globals:app-folder
};

declare function globals:app-collection() as xs:string {
  $globals:app-collection
};

(: deprecated :)
declare variable $globals:xcm-name := 'xcm';

declare function globals:doc-available( $name ) {
  fn:doc-available(fn:doc($globals:globals-uri)//Global[Key eq $name]/Value)
};

declare function globals:collection( $name ) {
  fn:collection(fn:doc($globals:globals-uri)//Global[Key eq $name]/Value)
};

declare function globals:doc( $name ) {
  fn:doc(fn:doc($globals:globals-uri)//Global[Key eq $name]/Value)
};


