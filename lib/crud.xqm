xquery version "1.0";

module namespace crud = "http://oppidoc.com/ns/xcm/crud";

import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../oppidum/lib/util.xqm";
import module namespace globals = "http://sbs.com/globals" at "globals.xqm";
import module namespace display = "http://oppidoc.com/ns/xcm/display" at "display.xqm";
import module namespace misc = "http://oppidoc.com/ns/xcm/misc" at "util.xqm";
import module namespace xal = "http://oppidoc.com/ns/xcm/xal" at "xal.xqm";
import module namespace user = "http://oppidoc.com/ns/xcm/user" at "user.xqm";
import module namespace utilities = "http://sbs.com/utilities" at "utilities.xqm";

declare function crud:get-document( $name as xs:string, $subject as element(), $lang as xs:string ) as element() {
  crud:get-document($name, $subject, (), $lang)
};

declare function crud:get-document( $name as xs:string, $subject as element(), $object as element()?, $lang as xs:string ) as element() {
  let $src := fn:collection($globals:templates-uri)//Template[@Mode eq 'read'][@Name eq $name]
  return
    if ($src) then
      util:eval(string-join($src/text(), ''))
    else
      oppidum:throw-error('CUSTOM', concat('Missing "', $name, '" template for read mode'))
};

declare function crud:save-document( $name as xs:string, $subject as element(), $form as element() ) as element() {
  crud:save-document($name, $subject, (), $form)
};

declare function crud:get-vanilla( $document as xs:string, $subject as element(), $object as element()?, $lang as xs:string ) as element() {
  let $src := globals:collection('templates-uri')//Template[@Mode eq 'read'][@Name eq 'vanilla']
  return
    if ($src) then
      misc:unreference(util:eval(string-join($src/text(), ''))) (: FIXME: $lang :)
    else
      oppidum:throw-error('CUSTOM', concat('Missing vanilla template for read mode'))
};

declare function crud:save-document(
  $name as xs:string, 
  $subject as element()?, 
  $object as element()?, 
  $form as element()? 
  ) as element()
{
  let $date := current-dateTime()
  let $uid := user:get-current-person-id() 
  let $src := fn:collection($globals:templates-uri)//Template[@Mode eq 'update'][@Name eq $name]
  return
    if ($src) then
      let $delta := misc:prune(util:eval(string-join($src/text(), '')))
(:      let $res := xal:apply-updates(if ($object) then $object else $subject, $delta):)
      let $res := xal:apply-updates($subject, $delta)
      return
        if (local-name($res) ne 'error') then
          oppidum:throw-message('ACTION-UPDATE-SUCCESS', ())
        else
          $res
    else
      oppidum:throw-error('CUSTOM', concat('Missing "', $name, '" template for update mode'))
};

(: ======================================================================
   FIXME: adopt generic $subject / $object convention
   ====================================================================== 
:)
declare function crud:save-vanilla(
  $document as xs:string, 
  $subject as element(), 
  $object as element(), 
  $form as element() 
  ) as element()
{
  let $date := current-dateTime()
  let $uid := user:get-current-person-id() 
  let $src := globals:collection('templates-uri')//Template[@Mode eq 'update'][@Name eq 'vanilla']
  return
    if ($src) then
      let $delta := misc:prune(util:eval(string-join($src/text(), '')))
      let $res := xal:apply-updates(if ($object) then $object else $subject, $delta)
      return
        if (local-name($res) ne 'error') then
          oppidum:throw-message('ACTION-UPDATE-SUCCESS', ())
        else
          $res
    else
      oppidum:throw-error('CUSTOM', concat('Missing vanilla template for update mode'))
};
