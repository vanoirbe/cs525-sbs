xquery version "3.1";
(: --------------------------------------
   XQuery Content Management Library

   Creator: Stéphane Sire <s.sire@oppidoc.fr>

   Access control functions
   Implements access control micro-language in application.xml

   Can be used :
   - to control display of command buttons in the user interface
   - fine grain access to CRUD controllers

   Conventions :
   - assert:check-* : high-level boolean functions to perform a check
   - access:assert-* : low-level interpretor functions

   Do not forget to also set mapping level <access> rules to prevent URL forgery !

   November 2016 - (c) Copyright 2016 Oppidoc SARL. All Rights Reserved.
   ----------------------------------------------- :)
module namespace access = "http://sbs.com/access";

declare namespace xdb = "http://exist-db.org/xquery/xmldb";
import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../oppidum/lib/util.xqm";
import module namespace globals = "http://sbs.com/globals" at "globals.xqm";
import module namespace user = "http://oppidoc.com/ns/xcm/user" at "user.xqm";

(: ======================================================================
   Implements access control rules in Documents element of application.xml
   Checks $action is allowed on document with root $root
   Interprets semantic rules against $subject and $object elements
   ======================================================================
:)
declare function access:rights-for-menu ($menu-item as xs:string, $action as xs:string) as xs:boolean
{
  let $access-model := fn:doc($globals:access-rigths-resource)//Menu
  let $conditions := $access-model/Item[@Name eq $menu-item]/Action[@Type=$action]
  let $username := xdb:get-current-user()
  let $person := fn:collection($globals:users-collection)//Person[UserProfile/Username = $username]/UserProfile
  return
    access:assert-conditions($conditions, $person,(), false())
};

declare function access:rights-for-menu ($menu-item as xs:string) as xs:boolean
{
  let $access-model := fn:doc($globals:access-rigths-resource)//Menu
  let $conditions := $access-model/Item[@Name eq $menu-item]
  let $username := xdb:get-current-user()
  let $person := fn:collection($globals:users-collection)//Person[UserProfile/Username = $username]/UserProfile
  return
    access:assert-conditions($conditions, $person,(), true())
};

(: ======================================================================
   Interprets access control rules micro-language (Meet | Avoid)* against 
   a subject and an object. 
   DEPRECATED: implementation function that should move to local: prefix
   You should call a access:check-*-permissions function.
   @return An xs:boolean
   ======================================================================
:)
declare function access:assert-conditions( 
  $conditions as element()?, 
  $person as element()?,
  $object as element()?,
  $default as xs:boolean
  ) as xs:boolean 
{
  if (empty($conditions/Meet)) then
    $default
  else 
    access:assert-users($conditions/Meet[@Condition = "users"], $person)
    or
    access:assert-roles($conditions/Meet[@Condition = "roles"], $person)
    or
    access:assert-expression($conditions/Meet[@Condition = "expression"], $person, $object)
};

declare function access:assert-users($users as xs:string?, $user-profile as element()?) as xs:boolean 
{
 if ($user-profile) then
   some $user in tokenize($users, " ") satisfies $user = $user-profile/Username/text()
 else false()
};

declare function access:assert-roles( 
  $roles as xs:string?,
  $person as element()?
  ) as xs:boolean 
{
 if ($person) then
   some $role in tokenize($roles, " ") satisfies $role = $person/Roles/Role/text()
 else false()
};

declare function access:assert-expression ($expression as xs:string?, $person as element()?, $object as element()?) as xs:boolean 
{
 if ($expression) then
   util:eval ($expression)
 else false()
};

(: ======================================================================
   Tests current user is compatible with semantic role given as parameter

   See also default email recipients list generation in workflow/alert.xql

   FIXME: should we complete $subject and $object with $profile 
   and get $profile from caller to factorize calls to user:get-user-profile() ?
   ======================================================================
:)
declare function access:user-has-role($role as xs:string) as xs:boolean {
  let $uid := user:get-current-person-id() 
  return
    if ($uid) then
      let $role := globals:doc('application-uri')/Application/Security/Roles/Role[@Name eq $suffix]/Meet
      return
        if ($role) then
          util:eval($role)
        else
          false()
    else
      false()
};