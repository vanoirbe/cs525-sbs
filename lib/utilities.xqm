module namespace utilities = "http://sbs.com/utilities";

import module namespace globals = "http://sbs.com/globals" at "globals.xqm";

(: ****************** 
   String utilities
   ****************** 
 :)

(: ======================================================================
   Normalizes a string to compare it with another one
   ======================================================================
:)
declare function utilities:normalize( $str as xs:string? ) as xs:string {
  translate(upper-case(translate(normalize-space($str),'àâäçéèêîôùû','aaaceeeiouu')),' ','')
};

(: ****************** 
   XML processing
   ****************** 
 :)
 
(: ======================================================================
   Removes namespaces in a given $xml-input
   ======================================================================
:)

(: inspired from author Arun Pareek :)
declare function utilities:strip-namespace ($xml-input as element()) as element(){
  element {xs:QName(local-name($xml-input ))}
  {
  for $child in $xml-input/(@*,node())
  return
    if ($child instance of element())
    then utilities:strip-namespace($child)
    else $child
  }
};

(: ======================================================================
   Returns node set containing only nodes in node set with textual
   content (note: attribute is not enough to qualify node for inclusion)
   ====================================================================== 
:)
declare function utilities:prune( $nodes as item()* ) as item()* {
  for $node in $nodes
  return
    typeswitch($node)
      case text()
        return $node
      case attribute()
        return $node
      case element()
        return
          if ($node/@_Prune eq 'none') then
            element { local-name($node) } { $node/attribute()[local-name(.) ne '_Prune'], $node/node() }
          else if (empty($node/*) and normalize-space($node) ne '') then (: LEAF node with text content :)
            $node
          else if (some $n in $node//* satisfies normalize-space($n) ne '') then
            let $tag := local-name($node)
            return
              element { $tag }
                { $node/attribute(), utilities:prune($node/node()) }
          else
            ()
      default
        return $node
};

declare function utilities:remove-empty-tags ($e as element()) as element() {
  element {local-name($e)}
    {utilities:prune($e/*)}
};

(: ======================================================================
   Returns a deep copy of the nodes sequence removing blacklisted node names
   ======================================================================
:)
declare function utilities:filter( $nodes as item()*, $blacklist as xs:string* ) as item()* {
  for $node in $nodes
  return
    typeswitch($node)
      case text()
        return $node
      case attribute()
        return $node
      case element()
        return
          if (local-name($node) = $blacklist) then
            ()
          else
            element { node-name($node) }
              { utilities:filter($node/(attribute()|node()), $blacklist) }
      default
        return $node
};

(: ********************** 
   Dictionaries utilities
   ********************** 
 :)

(: ======================================================================
   Returns a dictionary entry string for a given $lang and $key
   ======================================================================
:)

declare function utilities:get-dico-entry-for ($key as xs:string, $lang as xs:string) as xs:string{
  let $entry := fn:collection($globals:dico-collection)//*[local-name ()="Translations"][@lang =$lang]/*[local-name()="Translation"][@key=$key]/text()
  return
   if($entry) then
     $entry
   else
     concat('missing [', $key, ', lang="', $lang, '"]')
};


(: ======================================================================
   Ids management
   ======================================================================
:)

declare function utilities:get-value-of ( $element-name as xs:string ) as xs:positiveInteger {

 let $id :=  fn:doc ($globals:ids-resource)/Ids/node()[local-name () = $element-name]/text()
 return
 if ($id) then
   number($id)
 else
   ()
};

declare function utilities:inc-value-of ( $element-name as xs:string ) {
 let $id :=  fn:doc ($globals:ids-resource)/Ids/node()[local-name () = $element-name]/text()
 return
   update replace $id with $id+1
};

