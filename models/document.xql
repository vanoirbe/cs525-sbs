xquery version "1.0";
(: ------------------------------------------------------------------
   SBS

   Contributor(s): Christine Vanoirbeek

   Generic CRUD controller to manage a document 
 
   March 2019
   ---------------------------------------------------------------- :)

import module namespace request="http://exist-db.org/xquery/request";

import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../oppidum/lib/util.xqm";

import module namespace ajax = "http://oppidoc.com/oppidum/ajax" at "../lib/ajax.xqm";
import module namespace crud = "http://oppidoc.com/ns/xcm/crud" at "../lib/crud.xqm";

declare option exist:serialize "method=xml media-type=text/xml";

(: ======================================================================
   Validates submitted data.
   Returns a list of errors to report or the empty sequence.
   ======================================================================
:)
declare function local:validate-submission( $form as element() ) as element()* {
  let $errors := (
    )
  return $errors
};

let $m := request:get-method()
let $cmd := oppidum:get-command()
let $base-url := $cmd/@base-url
let $lang := string($cmd/@lang)
let $database := $cmd/resource/@db
let $collection := $cmd/resource/@collection
let $resource := $cmd/resource/@resource
let $goal := request:get-parameter('goal', 'read')
let $document-name := request:get-attribute('xquery.document')
let $document := fn:doc(concat($database,'/',$collection,'/',$resource))//*[name() = $document-name]
let $xal-template := request:get-attribute('xquery.xal-template')

return
  (: CV-FIXME: check access rights :)
  if (true()) then
    if ($m = 'POST') then
      let $form := oppidum:get-data()
      let $errors := local:validate-submission($form)
      return
        if (empty($errors)) then
          crud:save-document($xal-template, $document, $form)
        else
          ajax:report-validation-errors($errors)
    else (: assumes GET :)
      crud:get-document($xal-template, $document, $lang)
  else
    oppidum:throw-error('FORBIDDEN', ())
