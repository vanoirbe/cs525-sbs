xquery version "3.1";
(: ------------------------------------------------------------------
   SBS application
   
   Topic model 

   Contributor(s): Christine Vanoirbeek 

   July 2019
   ------------------------------------------------------------------ :)

import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../oppidum/lib/util.xqm";

declare option exist:serialize "method=xml media-type=text/xml";

let $content := fn:doc(oppidum:path-to-ref())

return 
  $content

