# Synopsis  : ./bootstrap.sh {admin-password}
# Parameter : database admin password
# ---
# Preconditions
# - eXist instance running
# - edit ../../../../client.properties to point to the running instance (port number, etc.)
# ---
# Creates initial /db/www/sbs/config and /db/www/qfs/mesh collections
# You should then use curl {home}/admin/deploy?t=[targets] to terminate the installation
# and then restore some application data / users from an application backup using {exist}/bin/backup.sh
#
APPCOL='sbs' # edit and put the same value as $globals:app-collection in globals.xqm
../../../../bin/client.sh -u admin -P $1 -m "/db/www/$APPCOL/mesh" --parse ../mesh/*.*html
../../../../bin/client.sh -u admin -P $1 -m "/db/www/$APPCOL/config" --parse ../config/*.xml

