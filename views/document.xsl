<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:site="http://oppidoc.com/oppidum/site" xmlns="http://www.w3.org/1999/xhtml">
  
  <xsl:output method="xml" media-type="text/html" omit-xml-declaration="yes" indent="yes"/>
  
  <xsl:param name="xslt.base-url">/</xsl:param>
  
  <xsl:template match="Root/Redirect">
    <site:view>
      <site:content>
    <script type="text/javascript">
      window.location="https://www.epfl.ch";
    </script>
      </site:content>
    </site:view>
  </xsl:template>
  
</xsl:stylesheet>