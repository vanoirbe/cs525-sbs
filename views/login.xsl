<?xml version="1.0" encoding="UTF-8"?>
<!-- Oppidum framework

    Login form generation

    Author: Stéphane Sire <s.sire@free.fr>

    Turns a <Login> model to a <site:content> module containing a login dialog
    box. Does nothing if the model contains a <Redirected> element (e.g. as a
    consequence of a successful login when handling a POST - see login.xql).

    July 2011
 -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:site="http://oppidoc.com/oppidum/site"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" media-type="text/html" omit-xml-declaration="yes" indent="yes"/>

  <!-- integrated URL rewriting... -->
  <xsl:param name="xslt.base-url"></xsl:param>

  <xsl:template match="/">
    <site:view>
      <xsl:apply-templates select="*"/>
    </site:view>
  </xsl:template>

  <!-- Login dialog box -->
  <xsl:template match="Login[not(Redirected)]">
    <site:content>
      <header class="masthead">
        <div class="container">
          <div class="intro-text">
            <form class="form-horizontal" action="{$xslt.base-url}login?url={To}" method="post">
              <fieldset>
              <legend loc="term.identification">Identification</legend>
                <div class="control-group">
                  <label class="control-label" for="login-user" loc="term.username">Nom d'utilisateur</label>
                  <div class="controls">
                    <input id="login-user" type="text" name="user" value="{User}"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="login-passwd" loc="term.password">Mot de passe</label>
                  <div class="controls">
                    <input id="login-passwd" type="password" name="password"/>
                  </div>
                </div>
                <div class="control-group" id="submit">
                  <div class="controls">
                    <input type="submit" class="btn"/>
                  </div>
                </div>
            </fieldset>
          </form>
          </div>
        </div>
      </header>
        
      
    </site:content>
  </xsl:template>

  <xsl:template match="Login[Redirected]">
    <p>Goto <a href="{Redirected}"><xsl:value-of select="Redirected"/></a></p>
  </xsl:template>

</xsl:stylesheet>
