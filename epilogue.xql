xquery version "3.1";
(: --------------------------------------
   SBS application

   Contributor(s): Christine Vanoirbeek 
   
   July 2019
   
   ------------------------------------------------------------------ :)

declare default element namespace "http://www.w3.org/1999/xhtml";

declare namespace site = "http://oppidoc.com/oppidum/site";
declare namespace xt = "http://ns.inria.org/xtiger";
declare namespace request = "http://exist-db.org/xquery/request";
declare namespace session = "http://exist-db.org/xquery/session";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace util="http://exist-db.org/xquery/util";
declare namespace xdb = "http://exist-db.org/xquery/xmldb";

import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../oppidum/lib/util.xqm";
import module namespace epilogue = "http://oppidoc.com/oppidum/epilogue" at "../oppidum/lib/epilogue.xqm";
import module namespace globals = "http://sbs.com/globals" at "lib/globals.xqm";
import module namespace access = "http://sbs.com/access" at "lib/access.xqm";
import module namespace view = "http://oppidoc.com/ns/xcm/view" at "lib/view.xqm";
(:import module namespace partial = "http://oppidoc.com/ns/xcm/partial" at "app/partial.xqm";:)

(: ======================================================================
   Trick to use request:get-uri behind a reverse proxy that injects
   /exist/projets/{$globals:app-collection} into the URL in production
   ======================================================================
:)
declare function local:my-get-uri ( $cmd as element() ) {
  concat($cmd/@base-url, $cmd/@trail, if ($cmd/@verb eq 'custom') then if ($cmd/@trail eq '') then $cmd/@action else concat('/', $cmd/@action) else ())
};

(: ======================================================================
   Typeswitch function
   -------------------
   Plug all the <site:{module}> functions here and define them below
   ======================================================================
:)
declare function site:branch( $cmd as element(), $source as element(), $view as element()* ) as node()*
{
 typeswitch($source)
 case element(site:skin) return view:skin($cmd, $view)
 case element(site:navigation) return site:navigation($cmd, $view)
 case element(site:footer) return site:footer($cmd, $view)
 case element(site:error) return view:error($cmd, $view)
 case element(site:message) return view:message($cmd)
 case element(site:login) return site:login($cmd)
 case element(site:lang) return site:lang($cmd, $view) 
 case element(site:field) return view:field($cmd, $source, $view)
 case element(site:conditional) return site:conditional($cmd, $source, $view)
 default return $view/*[local-name(.) = local-name($source)]/*
 (: default treatment to implicitly manage other modules :)
};

declare function local:gen-navigation ($base as xs:string, $lang as xs:string) as element()* {
 let $menu := fn:doc($globals:navigation-resource)//site:Menu
 let $user := xdb:get-current-user()
 return (
  
 for $i in $menu/site:Item
 return 
     if(access:rights-for-menu ($i/@Name, 'search')) then 
     <li class="nav-item">
       <a class="nav-link" href="{concat($base,'spaces/search')}">
       {$i/site:Labels/site:Label[@Lang = $lang]}
       </a>
     </li>
     else
     if (access:rights-for-menu($i/@Name, 'create')) then
     <li class="nav-item">
       <a class="nav-link" href="{concat($base,'spaces/create')}">
       {$i/site:Labels/site:Label[@Lang = $lang]}
       </a>
     </li>
   else
    if(access:rights-for-menu ($i/@Name)) then
     <li class="nav-item">
       <a class="nav-link" href="{concat($base,$i/site:Link,'')}">
       {$i/site:Labels/site:Label[@Lang = $lang]}
       </a>
     </li>
     else
       (),
     <li class="login">
       {xdb:get-current-user()}
       {if ( xdb:get-current-user() = 'guest') then 
         <a href="{concat($base,'login')}"> (Login)</a>
        else
         <a href="{concat($base,'logout')}"> (Logout)</a>
       }
     </li>
       )
};

(: ======================================================================
   Generates <site:navigation> menu
   ======================================================================
:)
declare function site:navigation( $cmd as element(), $view as element() ) as element()*
{
  let $base := string($cmd/@base-url)
  let $lang := $cmd/@lang
  return
    <nav class="navbar navbar-expand-lg bg-dark navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <ul class="navbar-nav text-uppercase ml-auto">
            {local:gen-navigation($base, $lang)}
          </ul>
          <i class="fas fa-bars"></i>
        </button>  
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            {local:gen-navigation($base, $lang)}
          </ul>
        </div>
      </div>
    </nav>
};

(: ======================================================================
   Generates <site:footer>
   ======================================================================
:)
declare function site:footer( $cmd as element(), $view as element() ) as element()*
{
 let 
   $footer :=
 <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <span class="copyright">Copyright Your Website 2019</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  return
  ($footer,
   site:login($cmd)
      )
};

(: ======================================================================
   Handles <site:login> LOGIN banner
   ======================================================================
:)
declare function site:login( $cmd as element() ) as element()*
{
 let
   $uri := local:my-get-uri($cmd),
   $user := oppidum:get-current-user()
 return
   if ($user = 'guest')  then
     if (not(ends-with($uri, '/login'))) then
       <a class="login" href="{$cmd/@base-url}login?url={$uri}">LOGIN</a>
     else
       <span>...</span>
   else
    let $user := if (string-length($user) > 7) then
                   if (substring($user,8,1) eq '-') then
                     substring($user, 1, 7)
                   else
                     concat(substring($user, 1, 7),'...')
                 else
                   $user
    return
      (
      <a href="{$cmd/@base-url}me" style="color:#333;text-decoration:none">{$user}</a>,
      <a class="login" href="{$cmd/@base-url}logout?url={$cmd/@base-url}" style="margin-left:10px">LOGOUT</a>
      )
};

(: ======================================================================
   Generates language menu
    - Simple logic so that default langauge (FR) is implicit (does not appear in URL)
   ======================================================================
:)
declare function site:lang( $cmd as element(), $view as element() ) as element()*
{
  let $lang := string($cmd/@lang)
  let $qs := request:get-query-string()
  let $uri := local:my-get-uri($cmd)
  return
    (
    if ($lang = 'en') then
      <span id="c-curLg">EN</span>
    else
      (: switching from 'de' or 'en' to default language 'fr' - that is removing language prefix from URL :)
      if (contains($uri, '/fr/')) then
        <a href="{replace(local:my-get-uri($cmd), 'fr/', '')}{if ($qs) then concat('?', $qs) else ()}" title="English">EN</a>
      else (: assumes en :)
        <a href="{replace(local:my-get-uri($cmd), 'ar/', '')}{if ($qs) then concat('?', $qs) else ()}" title="English">EN</a>,
    <span> | </span>,
    
    if ($lang = 'fr') then
      <span id="c-curLg">FR</span>
    else
      if (contains($uri, '/en/')) then
        <a href="{replace($uri, '/en/', '/fr/')}" title="Français">FR</a>
      else if (contains($uri, '/ar/')) then
        <a href="{replace($uri, '/ar/', '/fr/')}" title="Français">FR</a>
      else
        (: switching from default language 'fr' to 'en' - that is adding explicit language prefix to URL :)
        <a href="{replace($uri, concat("^",$cmd/@base-url), concat($cmd/@base-url, 'fr/'))}{if ($qs) then concat('?', $qs) else ()}" title="Français">FR</a>,
        <span> | </span>,
        
    if ($lang = 'ar') then
      <span id="c-curLg">AR</span>
    else
      if (contains($uri, '/en/')) then
        <a href="{replace($uri, '/en/', '/ar/')}" title="Arabe">AR</a>
      else if (contains($uri, '/fr/')) then
        <a href="{replace($uri, '/fr/', '/ar/')}" title="Arabe">AR</a>
      else
        (: switching from default language 'fr' to 'de' - that is adding explicit language prefix to URL :)
        <a href="{replace($uri, concat("^",$cmd/@base-url), concat($cmd/@base-url, 'ar/'))}{if ($qs) then concat('?', $qs) else ()}" title="Arabe">AR</a>,
    <span> | </span>
    
    )
};

(: ======================================================================
   Implements <site:conditional> in mesh files (e.g. rendering a Supergrid
   generated mesh XTiger template).

   Applies a simple logic to filter conditional source blocks.

   Keeps (/ Removes) the source when all these conditions hold true (logical AND):
   - @avoid does not match current goal (/ matches goal)
   - @meet matches current goal (/ does not match goal)
   - @flag is present in the request parameters  (/ is not present in parameters)
   - @noflag not present in request parameters (/ is present in parameters)

   TODO: move to view module with XQuery 3 (local:render as parameter)
   ======================================================================
:)
declare function site:conditional( $cmd as element(), $source as element(), $view as element()* ) as node()* {
  let $goal := request:get-parameter('goal', 'read')
  let $flags := request:get-parameter-names()
  return
    (: Filters out failing @meet AND @avoid and @noflag AND @flag :)
    if (not( 
               (not($source/@meet) or ($source/@meet = $goal))
           and (not($source/@avoid) or not($source/@avoid = $goal))
           and (not($source/@flag) or ($source/@flag = $flags))
           and (not($source/@noflag) or not($source/@noflag = $flags))
        )) 
    then
      ()
    else
      for $child in $source/node()
      return
        if ($child instance of element()) then
          (: FIXME: hard-coded 'site:' prefix we should better use namespace-uri
                    - currently limited to site:field :)
          if (starts-with(xs:string(node-name($child)), 'site:field')) then
            view:field($cmd, $child, $view)
          else
            local:render-iter($cmd, $child, $view)
        else
          $child
};

(: ======================================================================
   Recursive rendering function
   ----------------------------
   Copy this function as is inside your epilogue to render a mesh
   TODO: move to view module with XQuery 3 (site:branch as parameter)
   ======================================================================
:)
declare function local:render-iter( $cmd as element(), $source as element(), $view as element()* ) as element()
{
  element { node-name($source) }
  {
    $source/@*,
    for $child in $source/node()
    return
      if ($child instance of text()) then
        $child
      else
        (: FIXME: hard-coded 'site:' prefix we should better use namespace-uri :)
        if (starts-with(xs:string(node-name($child)), 'site:')) then
          (
            if (($child/@force) or
                ($view/*[local-name(.) = local-name($child)])) then
                 site:branch($cmd, $child, $view)
            else
              ()
          )
        else if ($child/*) then
          if ($child/@condition) then
          let $go :=
            if (string($child/@condition) = 'has-error') then
              oppidum:has-error()
            else if (string($child/@condition) = 'has-message') then
              oppidum:has-message()
            else if ($view/*[local-name(.) = substring-after($child/@condition, ':')]) then
                true()
            else
              false()
          return
            if ($go) then
              local:render-iter($cmd, $child, $view)
            else
              ()
        else
           local:render-iter($cmd, $child, $view)
        else
         $child
  }
};

(: ======================================================================
   Bootstraps template rendering
   Inserts lang attribute if its an html page
   ====================================================================== 
:)
declare function local:render( $cmd as element(), $source as element(), $view as element()* ) as element() {
  if (local-name($source) eq 'html') then
    element { node-name($source) }
      {
      $source/@*,
      $cmd/@lang,
      for $n in $source/*
      return local:render-iter($cmd, $n, $view)  
      }
  else
    local:render-iter($cmd, $source, $view)
};

(: ======================================================================
   Epilogue entry point
   ======================================================================
:)


let $mesh := epilogue:finalize()
let $cmd := request:get-attribute('oppidum.command')
let $sticky := false() (: TODO: support for forthcoming local:translation-agent() :)
let $lang := $cmd/@lang
let $dico := fn:collection($globals:dico-collection)//site:Translations[@lang = $lang]
let $isa_tpl := contains($cmd/@trail,"templates/") or ends-with($cmd/@trail,"/template")
let $maintenance := view:filter-for-maintenance($cmd, $isa_tpl)
return
  if ($mesh) then
    let $type := if (matches($cmd/@trail, "^test/|^calls/|activities/") or $isa_tpl) then
                      "method=html media-type=application/xhtml+xml"
                   else
                     "method=html5 media-type=text/html"
    let $page := local:render($cmd, $mesh, oppidum:get-data())
    return (
       util:declare-option("exist:serialize", concat($type, " encoding=utf-8 indent=yes")),
      view:localize($dico, $page, $sticky)
      )
  else
    view:localize($dico, oppidum:get-data(), $sticky)
