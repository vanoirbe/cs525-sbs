<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.ascc.net/xml/schematron">


  <!--<pattern name="unicity-constraints">
    <rule context="Data">
      <assert test="count(distinct-values(.//Asset/Id)) = count(.//Asset/Id)">Duplicate Ids for
        Assets</assert>
    </rule>
    <rule context="Asset">
      <assert test="(Id &gt;= 1) and (Id &lt;= //Ids/LastAssetId)">Unauthorized value for Id in
        Asset: should be in the interval 1 .. LastAssetId</assert>
    </rule>
    <rule context="Selector">
      <assert test="count(distinct-values(Option/child::*[1])) = count(Option/child::*[1])"
        >Duplicate key in selector</assert>
    </rule>
  </pattern>-->


  <!--<pattern name="valid-references">
    <rule context="Asset">
      <assert test="ProviderKey = ancestor::Data/Users/User/Id">Reference to unknown Provider from
        Asset</assert>
      <assert
        test="AssetCategoryRef = ancestor::Data/GlobalInformation/AssetCategories/AssetCategory/Id"
        >Pbl ref category</assert>
    </rule>
  </pattern>-->

  <pattern name="test">
    <rule context="Space">
      <assert test="Provider/Key">Reference to unknown Provider from
        Asset</assert>
    </rule>
  </pattern>

</schema>
