<grammar xmlns="http://relaxng.org/ns/structure/1.0" xmlns:ide="http://oppidum.com/ide"
  xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0"
  datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
  <start>
    <element name="Space" ide:store="resource" ide:resource-name="space">
      <element name="Id">
        <ref name="IdType"/>
      </element>
      <element name="Status">
        <ref name="SpaceStatusType"/>
      </element>
      <element name="Name">
        <data type="string"/>
      </element>
      <element name="Provider">
        <element name="Key">
          <ref name="IdType"/>
        </element>
      </element>
      <element name="Category">
        <element name="Ref">
          <data type="string"/>
        </element>
      </element>
      <element name="Capacity">
        <data type="positiveInteger"/>
      </element>
      <zeroOrMore>
        <element name="Description">
          <attribute name="Lang">
            <ref name="LangType"/>
          </attribute>
          <ref name="SimpleContentType"/>
        </element>
      </zeroOrMore>
      <optional>
        <element name="Photo">
          <ref name="ImageType"/>
        </element>
      </optional>
      <optional>
        <element name="Amenities">
          <oneOrMore>
            <element name="Amenity">
              <element name="ShortName">
                <data type="string"/>
              </element>
              <element name="Description">
                <ref name="ParagType"/>
              </element>
              <element name="PricePerHour">
                <ref name="AmountType"/>
              </element>
            </element>
          </oneOrMore>
        </element>
      </optional>
      <optional>
        <element name="Address">
          <ref name="AddressType"/>
        </element>
      </optional>
      <element name="PricePerHour">
        <ref name="AmountType"/>
      </element>
      <optional>
        <element name="Bookings">
          <oneOrMore>
            <element name="Booking">
              <element name="Booker">
                <element name="Ref">
                  <data type="string"/>
                </element>
              </element>
              <element name="Date">
                <data type="date"/>
              </element>
              <element name="Slots">
                <oneOrMore>
                  <element name="Slot">
                    <element name="From">
                      <data type="time"/>
                    </element>
                    <element name="To">
                      <data type="time"/>
                    </element>
                  </element>
                </oneOrMore>
              </element>
            </element>
          </oneOrMore>
        </element>
      </optional>
    </element>
  </start>
  <define name="IdType">
    <data type="nonNegativeInteger"/>
  </define>
  <define name="LangType">
    <choice>
      <value>en</value>
      <value>fr</value>
    </choice>
  </define>
  <define name="ParagType">
    <zeroOrMore>
      <choice>
        <element name="Fragment">
          <ref name="FragmentType"/>
        </element>
        <element name="Link">
          <ref name="LinkType"/>
        </element>
      </choice>
    </zeroOrMore>
  </define>
  <define name="FragmentKindType">
    <choice>
      <value>important</value>
      <value>emphasize</value>
    </choice>
  </define>
  <define name="FragmentType">
    <optional>
      <attribute name="FragmentKind">
        <ref name="FragmentKindType"/>
      </attribute>
    </optional>
    <data type="string"/>
  </define>
  <define name="LinkType">
    <element name="LinkText">
      <data type="string"/>
    </element>
    <element name="LinkRef">
      <data type="string"/>
    </element>
  </define>
  <define name="ListType">
    <optional>
      <attribute name="Style">
        <ref name="ListStyleType"/>
      </attribute>
    </optional>
    <optional>
      <element name="ListHeader">
        <data type="string"/>
      </element>
    </optional>
    <oneOrMore>
      <element name="Item">
        <oneOrMore>
          <element name="Parag">
            <ref name="ParagType"/>
          </element>
        </oneOrMore>
      </element>
    </oneOrMore>
  </define>
  <define name="SimpleContentType">
    <zeroOrMore>
      <choice>
        <element name="Title">
          <data type="string"/>
        </element>
        <element name="Parag">
          <ref name="ParagType"/>
        </element>
        <element name="List">
          <ref name="ListType"/>
        </element>
      </choice>
    </zeroOrMore>
  </define>
  <define name="ListStyleType">
    <choice>
      <value>decimal</value>
    </choice>
  </define>
  <define name="ImageType">
    <element name="Name">
      <data type="string"/>
    </element>
    <optional>
      <oneOrMore>
        <element name="Caption">
          <attribute name="Lang">
            <ref name="LangType"/>
          </attribute>
          <data type="string"/>
        </element>
      </oneOrMore>
    </optional>
  </define>
  <define name="AddressType">
    <element name="StreetNameAndNo">
      <data type="string"/>
    </element>
    <element name="Town">
      <data type="string"/>
    </element>
    <element name="PostalCode">
      <data type="string"/>
    </element>
    <element name="Country">
      <element name="Ref">
        <data type="string"/>
      </element>
    </element>
  </define>
  <define name="SpaceStatusType">
    <choice>
      <value>Running</value>
      <value>Archived</value>
    </choice>
  </define>
  <define name="AttributeLang">
    <attribute name="Lang">
      <ref name="LangType"/>
    </attribute>
  </define>
  <define name="AmountType">
    <element name="Value">
      <data type="decimal"/>
    </element>
    <element name="Currency">
      <element name="Ref">
        <data type="string"/>
      </element>
    </element>
  </define>
  <define name="CurrencyType">
    <choice>
      <value>CHF</value>
      <value>EUR</value>
    </choice>
  </define>
  <define name="PersonNameType">
    <element name="FirstName">
      <data type="string"/>
    </element>
    <element name="LastName">
      <data type="string"/>
    </element>
  </define>
  <define name="HourType">
    <data type="integer">
      <param name="minInclusive">7</param>
      <param name="maxInclusive">22</param>
    </data>
  </define>


</grammar>
