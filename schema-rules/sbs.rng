<grammar xmlns="http://relaxng.org/ns/structure/1.0"
  xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0" xmlns:ide="http://oppidum.com/ide"
  datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
  <start>
    <element name="SBS" ide:appli="SBS">
      <element name="GlobalInformation" ide:store="collection"
        ide:collection-name="global-information">
        <element name="AccessRights">
          <text/>
        </element>
        <element name="Navigation">
          <text/>
        </element>
        <element name="Selectors">
          <zeroOrMore>
            <element name="Selectors">
              <attribute name="Lang">
                <ref name="LangType"/>
              </attribute>
              <oneOrMore>
                <element name="Selector">
                  <attribute name="Name">
                    <data type="string"/>
                  </attribute>
                  <attribute name="Value">
                    <data type="string"/>
                  </attribute>
                  <attribute name="Label">
                    <data type="string"/>
                  </attribute>
                  <oneOrMore>
                    <element name="Option">
                      <text/>
                    </element>
                  </oneOrMore>
                </element>
              </oneOrMore>
            </element>
          </zeroOrMore>
        </element>
        <element name="Ids" ide:store="resource" ide:resource-name="ids">
          <interleave>
            <element name="LastSpaceId">
              <ref name="IdType"/>
            </element>
            <element name="LastUserId">
              <ref name="IdType"/>
            </element>
            <element name="LastBookingId">
              <ref name="IdType"/>
            </element>
          </interleave>
        </element>
      </element>
      <element name="Topics" ide:store="collection" ide:store-collection="topics">
        <element name="Home" ide:store="resource" ide:resource-name="home">
          <element name="Title">
            <data type="string"/>
          </element>
          <element name="SubTitle">
            <data type="string"/>
          </element>
          <element name="SearchSpaceIcon">
            <ref name="ImageType"/>
          </element>
          <element name="ShareSpaceIcon">
            <ref name="ImageType"/>
          </element>
          <element name="Content">
            <element name="SearchSpace">
              <element name="Title">
                <data type="string"/>
              </element>
              <oneOrMore>
                <element name="Parag">
                  <ref name="ParagType"/>
                </element>
              </oneOrMore>
            </element>
            <element name="ShareSpace">
              <element name="Title">
                <data type="string"/>
              </element>
              <oneOrMore>
                <element name="Parag">
                  <ref name="ParagType"/>
                </element>
              </oneOrMore>
            </element>
          </element>
        </element>
        <element name="SpaceSearching" ide:store="resource" ide:resource-name="space-searching">
          <element name="Content">
            <ref name="SimpleContentType"/>
          </element>
        </element>
        <element name="SpaceSharing" ide:store="resource" ide:resource-name="space-sharing">
          <element name="Content">
            <ref name="SimpleContentType"/>
          </element>
        </element>
      </element>
      <element name="Spaces" ide:store="collection" ide:collection-name="spaces">
        <zeroOrMore>
          <element name="Space" ide:store="resource" ide:resource-name="space">
            <element name="Id">
              <ref name="IdType"/>
            </element>
            <element name="Status">
              <ref name="SpaceStatusType"/>
            </element>
            <element name="Name">
              <data type="string"/>
            </element>
            <element name="Provider">
              <element name="Key">
                <ref name="IdType"/>
              </element>
            </element>
            <element name="Category">
              <data type="string"/>
            </element>
            <element name="Capacity">
              <data type="positiveInteger"/>
            </element>
            <element name="Description">
              <data type="string"/>
            </element>
            <optional>
              <element name="Photo">
                <ref name="ImageType"/>
              </element>
            </optional>
            <optional>
              <element name="Amenities">
                <oneOrMore>
                  <element name="Amenity">
                    <element name="ShortName">
                      <data type="string"/>
                    </element>
                    <element name="Description">
                      <data type="string"/>
                    </element>
                    <element name="PricePerHour">
                      <data type="positiveInteger"/>
                    </element>
                  </element>
                </oneOrMore>
              </element>
            </optional>
            <element name="Address">
              <ref name="AddressType"/>
            </element>
            <element name="PricePerHour">
              <data type="positiveInteger"/>
            </element>
            <optional>
              <element name="Bookings">
                <oneOrMore>
                  <element name="Booking">
                    <element name="Booker">
                      <element name="Key">
                        <data type="string"/>
                      </element>
                    </element>
                    <element name="Date">
                      <data type="date"/>
                    </element>
                    <element name="Slots">
                      <oneOrMore>
                        <element name="Slot">
                          <element name="From">
                            <data type="time"/>
                          </element>
                          <element name="To">
                            <data type="time"/>
                          </element>
                        </element>
                      </oneOrMore>
                    </element>
                  </element>
                </oneOrMore>
              </element>
            </optional>
          </element>
        </zeroOrMore>
      </element>
      <element name="Users" ide:store="collection" ide:collection-name="users">
        <element name="Persons" ide:store="resource" ide:resource-name="persons">
          <zeroOrMore>
            <element name="Person">
              <element name="Id">
                <ref name="IdType"/>
              </element>
              <element name="Information">
                <element name="Name">
                  <ref name="PersonNameType"/>
                </element>
                <element name="Email">
                  <data type="string"/>
                </element>
              </element>
              <element name="UserProfile">
                <element name="Username">
                  <data type="string"/>
                </element>
                <optional>
                  <element name="Roles">
                    <oneOrMore>
                      <element name="Role">
                        <data type="string"/>
                      </element>
                    </oneOrMore>
                  </element>
                </optional>
              </element>
            </element>
          </zeroOrMore>
        </element>
      </element>
    </element>
  </start>
  <define name="IdType">
    <data type="nonNegativeInteger"/>
  </define>
  <define name="LangType">
    <choice>
      <value>en</value>
      <value>fr</value>
    </choice>
  </define>
  <define name="ParagType">
    <zeroOrMore>
      <choice>
        <element name="Fragment">
          <ref name="FragmentType"/>
        </element>
        <element name="Link">
          <ref name="LinkType"/>
        </element>
      </choice>
    </zeroOrMore>
  </define>
  <define name="FragmentKindType">
    <choice>
      <value>important</value>
      <value>emphasize</value>
    </choice>
  </define>
  <define name="FragmentType">
    <optional>
      <attribute name="FragmentKind">
        <ref name="FragmentKindType"/>
      </attribute>
    </optional>
    <data type="string"/>
  </define>
  <define name="LinkType">
    <element name="LinkText">
      <data type="string"/>
    </element>
    <element name="LinkRef">
      <data type="string"/>
    </element>
  </define>
  <define name="ListType">
    <optional>
      <attribute name="Style">
        <ref name="ListStyleType"/>
      </attribute>
    </optional>
    <optional>
      <element name="ListHeader">
        <data type="string"/>
      </element>
    </optional>
    <oneOrMore>
      <element name="Item">
        <oneOrMore>
          <element name="Parag">
            <ref name="ParagType"/>
          </element>
        </oneOrMore>
      </element>
    </oneOrMore>
  </define>
  <define name="SimpleContentType">
    <zeroOrMore>
      <choice>
        <element name="Title">
          <data type="string"/>
        </element>
        <element name="Parag">
          <ref name="ParagType"/>
        </element>
        <element name="List">
          <ref name="ListType"/>
        </element>
      </choice>
    </zeroOrMore>
  </define>
  <define name="ListStyleType">
    <choice>
      <value>decimal</value>
    </choice>
  </define>
  <define name="ImageType">
    <element name="Name">
      <data type="string"/>
    </element>
    <optional>
      <element name="Caption">
        <data type="string"/>
      </element>
    </optional>
  </define>
  <define name="AddressType">
    <element name="StreetNameAndNo">
      <data type="string"/>
    </element>
    <element name="Town">
      <data type="string"/>
    </element>
    <element name="PostalCode">
      <data type="string"/>
    </element>
  </define>
  <define name="SpaceStatusType">
    <choice>
      <value>Running</value>
      <value>Archived</value>
    </choice>
  </define>
  <define name="CurrencyType">
    <choice>
      <value>CHF</value>
      <value>EUR</value>
    </choice>
  </define>
  <define name="PersonNameType">
    <element name="FirstName">
      <data type="string"/>
    </element>
    <element name="LastName">
      <data type="string"/>
    </element>
  </define>
</grammar>
