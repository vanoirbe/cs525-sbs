xquery version "1.0";

import module namespace validation="http://exist-db.org/xquery/validation";
import module namespace util="http://exist-db.org/xquery/util";
import module namespace globals="http://sbs.com/globals" at "../../lib/globals.xqm";

declare option exist:serialize "method=xml media-type=text/xml";

declare function local:validate-resources ($resource-name as xs:string, $collection as xs:string, $schema-name as xs:string) {
let $schema := fn:doc(concat($globals:schema-collection, '/',$schema-name,'.rng'))
return
  for $r in fn:collection($collection)//*[local-name(.) eq $resource-name]
  let $report := validation:jing-report($r, $schema)
  return
     if ($report/status='invalid') then
     (
     <Name>{$r/local-name()}</Name>,
     $r,
     $report/message
     ) 
     else()
};


<Report>
  {local:validate-resources('Space', $globals:spaces-collection, 'space')}
  {local:validate-resources('Home', $globals:topics-collection, 'home')}
  {local:validate-resources('SpaceSearching', $globals:topics-collection, 'space-searching')}
  {local:validate-resources('SpaceSharing', $globals:topics-collection, 'space-sharing')}
  {local:validate-resources('Persons', $globals:users-collection, 'persons')}
</Report>