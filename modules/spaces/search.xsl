<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:site="http://oppidoc.com/oppidum/site" xmlns="http://www.w3.org/1999/xhtml">
  
  <xsl:output method="xml" media-type="text/html" omit-xml-declaration="yes" indent="yes"/>
  
  <xsl:param name="xslt.base-url">/</xsl:param>
  
  <xsl:template match="Display">
    <site:view>
      <site:content>
        <section id="about">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <h3 class="section-heading text-uppercase">Search spaces</h3>
                <xsl:apply-templates select="Form"/>
              </div>
            </div>
          </div>
        </section>
      </site:content>
    </site:view>
  </xsl:template>
  
  <xsl:template match="Spaces">
    <div id="results">
      <section class="bg-light" id="portfolio">
        <div class="container">
          <div class="row">
            <xsl:apply-templates select = "Space"/>
          </div>
        </div>
      </section>
    </div>
  </xsl:template>
  
  <xsl:template match="Space">
    <xsl:variable name="search-date" select="parent::Spaces/SearchRequest/Date"/>
    <xsl:variable name="from" select="parent::Spaces/SearchRequest/From"/>
    <xsl:variable name="to" select="parent::Spaces/SearchRequest/To"/>
    <div class="col-md-4 col-sm-6 portfolio-item">
      <div class="portfolio-caption">
        <h5><xsl:value-of select="Address/Town"/></h5>
        <p class="text-muted"><xsl:value-of select="Name"/></p>
      </div>
      <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
        <div class="portfolio-hover">
          <div class="portfolio-hover-content">
            <i class="fas fa-plus fa-3x"></i>
          </div>
        </div>
        <img class="img-fluid" src="{$xslt.base-url}/images/{Photo/Name}" alt=""/>
      </a>
      <p>Capacity: <xsl:value-of select="Capacity"/> persons</p>
      <p>Price per hour: <xsl:value-of select="PricePerHour"/> CHF 
        <a class="btn-primary" 
          href="{$xslt.base-url}spaces/{Id}/book?date={$search-date}&amp;from={$from}&amp;to={$to}">Book</a>
      </p>
    </div>
  </xsl:template>
  
  <xsl:template match="Form">
    <div id="c-editor-{@Id}" class="c-autofill-border" data-template="{Template}">
      <noscript loc="app.message.js">Activez Javascript</noscript>
      <p loc="app.message.loading">Chargement du formulaire en cours</p>
    </div>
    <button class="btn btn-primary" data-command="reset" data-target="c-editor-{@Id}"
      loc="action.reset">Effacer</button>
    <button class="btn btn-primary" data-command="save" data-target="c-editor-{@Id}"  
      data-replace-target="results" data-src="{$xslt.base-url}spaces/search" loc="action.search">Search</button>
    <xsl:apply-templates select="Actions"/>
    <div id="results">
      <xsl:apply-templates select="Spaces"/>
    </div>
  </xsl:template> 
  
  <xsl:template match="Actions/Cancel">
    <a class="btn btn-primary" href="{$xslt.base-url}{Redirect}" loc="action.cancel">Cancel</a>
  </xsl:template>
 
</xsl:stylesheet>


