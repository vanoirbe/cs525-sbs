xquery version "1.0";
(: ------------------------------------------------------------------
   SBS

   Contributor(s): Christine Vanoirbeek 

   Space CRUD controller  
 
   October 2019 
   ---------------------------------------------------------------- :)

import module namespace request="http://exist-db.org/xquery/request";

import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../../oppidum/lib/util.xqm";

import module namespace ajax = "http://oppidoc.com/oppidum/ajax" at "../../lib/ajax.xqm";
import module namespace crud = "http://oppidoc.com/ns/xcm/crud" at "../../lib/crud.xqm";

import module namespace globals ="http://sbs.com/globals" at "../../lib/globals.xqm";
import module namespace utilities ="http://sbs.com/utilities" at "../../lib/utilities.xqm";


declare option exist:serialize "method=xml media-type=text/xml";

(: ======================================================================
   Validates submitted data.
   Returns a list of errors to report or the empty sequence.
   ======================================================================
:)
declare function local:validate-submission( $form as element() ) as element()* {
  let $report := validation:jing-report($form, fn:doc('/db/www/sbs/validation/forms/form-create-space.rng'))
  let $errors:= if ($report/status = 'invalid') then $report else () 
  return $errors
};

let $m := request:get-method()
let $cmd := oppidum:get-command()
let $base-url := $cmd/@base-url
let $lang := string($cmd/@lang)
let $xal-template := 'space'
return
    if ($m = 'POST') then
      let $form := oppidum:get-data()
      let $errors := local:validate-submission($form)
      return
        if (empty($errors)) then
          oppidum:throw-error('NOT-IMPLEMENTED-YET',())
        else
          ajax:report-validation-errors($errors)
    else (: assumes GET :)
      oppidum:throw-error('NOT-IMPLEMENTED-YET',())