xquery version "1.0";
(: ------------------------------------------------------------------
   SBS

   Contributor(s): Christine Vanoirbeek 

   October 2019 
   ---------------------------------------------------------------- :)

import module namespace request="http://exist-db.org/xquery/request";
import module namespace validation="http://exist-db.org/xquery/validation";

import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../../oppidum/lib/util.xqm";
import module namespace ajax = "http://oppidoc.com/oppidum/ajax" at "../../lib/ajax.xqm";

import module namespace globals ="http://sbs.com/globals" at "../../lib/globals.xqm";
import module namespace utilities ="http://sbs.com/utilities" at "../../lib/utilities.xqm";


declare option exist:serialize "method=xml media-type=text/xml";

(: ======================================================================
   Validates submitted data.
   Returns a list of errors to report or the empty sequence.
   ======================================================================
:)
declare function local:validate-submission( $form as element() ) as element()* {
  let $report := validation:jing-report($form, fn:doc('/db/www/sbs/validation/forms/form-search-space.rng'))
  let $errors:= if ($report/status = 'invalid') then $report else () 
  return $errors
};

declare function local:overlaps ($a-start as xs:time, $a-end as xs:time, $b-start as xs:time, $b-end as xs:time) as xs:boolean{
    ($a-end > $b-start) and ($a-start < $b-end)
};

declare function local:search-spaces( $request as element() ) as element()* {
  let $category := $request/Category
  let $capacity := xs:integer($request/Capacity/text())
  let $town := utilities:normalize($request/Town/text())
  let $date := $request/Date
  let $from := xs:time($request/From/text())
  let $to := xs:time($request/To/text())
  return
      for $s in fn:collection($globals:spaces-collection)//Space where
          ($s/Category = $category)
      and (utilities:normalize($s/Address/Town/text()) = $town)
      and (xs:integer($s/Capacity/text()) ge $capacity)
      and (
          (empty($s/Bookings)) or
          ((not(empty($s/Bookings))) and
          (every $slot in $s/Bookings/Booking[Date = $date]/Slots/Slot satisfies (not(local:overlaps($slot/From, $slot/To, $from, $to)))))
          )
      return
        $s
};

let $m := request:get-method()
let $cmd := oppidum:get-command()
let $base-url := $cmd/@base-url
return
    if ($m = 'POST') then
      let $form := utilities:remove-empty-tags(oppidum:get-data())
      let $errors := local:validate-submission($form)
      return
      if (empty($errors)) then
          <Spaces>
            {local:search-spaces($form)}
            <SearchRequest>
              {$form/Date}
              {$form/From}
              {$form/To}
            </SearchRequest>
          </Spaces>
      else 
         ajax:report-validation-errors($errors)
    else (: assumes GET :)
      <Display>
         <Form Id="SpaceSearch">
           <Template>{concat($base-url,'templates/space-search?goal=create')}</Template>
           <Actions>
             <Cancel>
               <Redirect>home</Redirect>
             </Cancel>
           </Actions>
         </Form>
     </Display>
 