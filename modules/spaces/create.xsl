<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:site="http://oppidoc.com/oppidum/site" xmlns="http://www.w3.org/1999/xhtml">
  
  <xsl:output method="xml" media-type="text/html" omit-xml-declaration="yes" indent="yes"/>
  
  <xsl:param name="xslt.base-url">/</xsl:param>
  
  <xsl:template match="/">
    <site:view>
      <site:content>
        <section id="about">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <h3 class="section-heading text-uppercase">New space</h3>
                <xsl:apply-templates select="Display/Form"/>
              </div>
            </div>
          </div>
        </section>
      </site:content>
    </site:view>
  </xsl:template>
  
  <xsl:template match="Form">
    <div id="c-editor-{@Id}" class="c-autofill-border" data-template="{Template}">
      <noscript loc="app.message.js">Activez Javascript</noscript>
      <p loc="app.message.loading">Chargement du formulaire en cours</p>
    </div>
    <button class="btn btn-primary" data-command="reset" data-target="c-editor-{@Id}"
      loc="action.reset">Effacer</button>
    <button class="btn btn-primary" data-command="save" data-target="c-editor-{@Id}" data-src="{$xslt.base-url}spaces/create?goal=create" 
      data-replace-type="event" loc="action.create">Create</button>
    <xsl:apply-templates select="Actions"/> 
  </xsl:template> 
  
  <xsl:template match="Actions/Cancel">
    <a class="btn btn-primary" href="{$xslt.base-url}{Redirect}" loc="action.cancel">Annuler</a>
  </xsl:template>

</xsl:stylesheet>


