(: -------------------------------------------
   SBS

   Contributor(s): Christine Vanoirbeek

   October 2019
   ----------------------------------------- :)
   
import module namespace request="http://exist-db.org/xquery/request";
import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../../oppidum/lib/util.xqm";

declare option exist:serialize "method=xml media-type=text/xml";

let $cmd := oppidum:get-command()
let $lang := string($cmd/@lang)
let $base-url := $cmd/@base-url

return 
<Display>
  <Form Id="Space">
    <Template>{concat($base-url,'templates/space?goal=create')}</Template>
    <Actions>
      <Cancel>
        <Redirect>home</Redirect>
      </Cancel>
    </Actions>
  </Form>
</Display>