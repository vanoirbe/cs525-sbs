xquery version "3.1";
(: -------------------------------------------------
   SBS

   Contributor(s): Christine Vanoirbeek

   Generates extension points for space forms

   September 2019 
   ----------------------------------------------- :)

declare default element namespace "http://www.w3.org/1999/xhtml";

import module namespace request="http://exist-db.org/xquery/request";
import module namespace form = "http://oppidoc.com/oppidum/form" at "../../lib/form.xqm";
import module namespace display = "http://oppidoc.com/ns/xcm/display" at "../../lib/display.xqm";

import module namespace oppidum = "http://oppidoc.com/oppidum/util" at "../../../oppidum/lib/util.xqm";

declare namespace xt = "http://ns.inria.org/xtiger";
declare namespace site = "http://oppidoc.com/oppidum/site";

declare option exist:serialize "method=xml media-type=text/xml";

let $cmd := request:get-attribute('oppidum.command')
let $lang := string($cmd/@lang)
let $target := oppidum:get-resource(oppidum:get-command())/@name
let $goal := request:get-parameter('goal', 'create')
return
  if ($goal = 'create') then 
  <site:view>
    <site:field Key="category">
      {form:gen-selector-for('Category',$lang,";multiple=no;typeahead=yes")}
    </site:field>
  </site:view>  
else 
  <site:view>
    <site:field Key="category">
      {form:gen-selector-for('Category',$lang,";multiple=no;typeahead=yes")}
    </site:field>
  </site:view>
