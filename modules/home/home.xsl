<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:site="http://oppidoc.com/oppidum/site" xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" media-type="text/html" omit-xml-declaration="yes" indent="yes"/>

  <xsl:param name="xslt.base-url">/</xsl:param>
  
  <xsl:template match="/">
    <site:view>
      <site:content>
        <xsl:apply-templates select="*"/>
      </site:content>
    </site:view>
  </xsl:template>
  
  <xsl:template match="Home">
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase"><xsl:value-of select="Title"/></h2>
            <h3 class="section-subheading text-muted"><xsl:value-of select="SubTitle"/></h3>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-6">
            <img src="{$xslt.base-url}/images/{SearchSpaceIcon/Name}" alt=""/>
            <h4 class="service-heading"><xsl:value-of select="Content/SearchSpace/Title"/></h4>
            <xsl:apply-templates select="Content/SearchSpace/Parag"></xsl:apply-templates>
          </div>
          <div class="col-md-6">
            <img src="{$xslt.base-url}/images/{ShareSpaceIcon/Name}" alt=""/>
            <h4 class="service-heading"><xsl:value-of select="Content/ShareSpace/Title"/></h4>
            <xsl:apply-templates select="Content/ShareSpace/Parag"></xsl:apply-templates>
          </div>
        </div>
      </div>
    </section>
  </xsl:template>
  
  <xsl:template match="Parag">
    <p class="text-muted"><xsl:value-of select="."/></p>
  </xsl:template>
   
</xsl:stylesheet>
